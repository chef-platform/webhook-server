# frozen_string_literal: true

name 'webhook-server'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/webhook-server@incoming.gitlab.com'
license 'Apache-2.0'
description 'Install and configure webhook server'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/webhook-server'
issues_url 'https://gitlab.com/chef-platform/webhook-server/issues'
version '1.4.0'

supports 'centos', '>= 7.1'

depends 'ark'

chef_version '>= 12.19'
