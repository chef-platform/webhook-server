# frozen_string_literal: true

#
# Copyright (c) 2021 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
require 'json'

hooks_config = [{
  'id': 'redeploy-webhook',
  'execute-command': '/var/scripts/redeploy.sh',
  'command-working-directory': '/var/webhook'
}]

describe file('/opt/webhook/hooks.json') do
  it { should contain(JSON.pretty_generate(hooks_config)) }
end

cli_opts = {
  'hooks' => '/opt/webhook/hooks.json',
  'header' => ['header1' => 'value1', 'header2' => 'value2']
}.map do |key, opts|
  [opts].flatten.map do |opt|
    "-#{key.to_s.tr('_', '-')} #{opt}" unless opt == 'nil'
  end
end.flatten.join(" \\\n  ")

describe file('/etc/systemd/system/webhook.service') do
  it { should contain(cli_opts) }
end

describe file('/opt/bin/webhook') do
  it { should be_executable }
end

describe service('webhook') do
  it { should be_enabled }
  it { should be_running.under('systemd') }
end

describe port(9000) do
  it { should be_listening }
end
