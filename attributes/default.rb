# frozen_string_literal: true

#
# Copyright (c) 2021 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

cookbook_name = 'webhook-server'

# Webhook version and platform
default[cookbook_name]['version'] = '2.8.0'
version = node[cookbook_name]['version']

default[cookbook_name]['platform'] = 'linux-amd64'
platform = node[cookbook_name]['platform']

# Where to get webhook archive
default[cookbook_name]['mirror_base'] =
  'https://github.com/adnanh/webhook/releases/download'
mirror_base = node[cookbook_name]['mirror_base']

default[cookbook_name]['mirror'] =
  "#{mirror_base}/#{version}/webhook-#{platform}.tar.gz"

# Webhook archive sha256 checksum
default[cookbook_name]['checksum'] =
  '4591916aa85a3332af3f5733d0d75ce4e065aa184dbda7b945bb2542eba7da4b'

# Ark resource configuration
default[cookbook_name]['prefix_root'] = '/opt' # base installation dir
default[cookbook_name]['prefix_home'] = '/opt' # where is link to install dir
default[cookbook_name]['prefix_bin'] = '/opt/bin' # where to link binaries
prefix_home = node[cookbook_name]['prefix_home']

# User and group of webhook process
default[cookbook_name]['user'] = 'webhook'
default[cookbook_name]['group'] = 'webhook'

# Hooks configuration
default[cookbook_name]['hooks'] = []

# Webhook options
# "key => value" will be transformed to "-key value" in webhook cli
# For keys without value like 'hotreload', set value to empty string
# To unset a key, set it to 'nil' (the string)
# To set a key multiple time, use an array
default[cookbook_name]['options'] = {
  'hooks' => "#{prefix_home}/webhook/hooks.json"
}

# Systemd service unit
default[cookbook_name]['unit'] = {
  'Unit' => {
    'Description' => 'webhook server',
    'After' => 'network.target'
  },
  'Service' => {
    'Type' => 'simple',
    'User' => node[cookbook_name]['user'],
    'Group' => node[cookbook_name]['group'],
    'Restart' => 'on-failure',
    'ExecStart' => 'TO_BE_COMPLETED'
  },
  'Install' => {
    'WantedBy' => 'multi-user.target'
  }
}

# If webhook service is restarted when config file changes
default[cookbook_name]['auto_restart'] = true
