# frozen_string_literal: true

#
# Copyright (c) 2021 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Construct unit content from command line options
cli_opts = node[cookbook_name]['options'].map do |key, opts|
  [opts].flatten.map do |opt|
    "-#{key.to_s.tr('_', '-')} #{opt}" unless opt == 'nil'
  end
end.flatten.join(" \\\n  ")

bin = "#{node[cookbook_name]['prefix_bin']}/webhook"
unit = node[cookbook_name]['unit'].to_h
unit['Service']['ExecStart'] = "#{bin} #{cli_opts}"

# Configure systemd unit
systemd_unit 'webhook.service' do
  content unit
  action %i[create enable start]
end
